package tech.spring.security.csrf.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class GreetingResource {

    @GetMapping("hello")
    public String greet() {
        return "Hello from Spring Security with csrf protection";
    }

    @PostMapping("without-csrf")
    public String postCsrfDisable() {
        return "Hello from Spring Security with csrf protection disable";
    }

    @PostMapping("with-csrf")
    public String postCsrfEnable() {
        return "Hello from Spring Security with csrf protection enable";
    }
}
