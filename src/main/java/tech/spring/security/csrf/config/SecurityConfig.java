package tech.spring.security.csrf.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import tech.spring.security.csrf.config.csrf.CustomCsrfTokenRepository;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final CustomCsrfTokenRepository csrfTokenRepository;

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().anyRequest().permitAll();

        http.csrf(csrfCustomizer -> {
            csrfCustomizer.csrfTokenRepository(csrfTokenRepository);
            csrfCustomizer.ignoringAntMatchers("/without-csrf");
        }).formLogin();
    }
}
