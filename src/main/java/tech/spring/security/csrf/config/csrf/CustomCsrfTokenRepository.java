package tech.spring.security.csrf.config.csrf;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

@Configuration
@Data
public class CustomCsrfTokenRepository implements CsrfTokenRepository {

    private final JpaTokenRepository jpaTokenRepository;

    @Override
    public CsrfToken generateToken(HttpServletRequest httpServletRequest) {
        UUID token = UUID.randomUUID();
        return new DefaultCsrfToken("X-CSRF-TOKEN", "_csrf", token.toString());
    }

    @Override
    public void saveToken(CsrfToken csrfToken, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String identifier = httpServletRequest.getHeader("X-IDENTIFIER");

        Optional<Token> optionalToken = jpaTokenRepository.getTokenByIdentifier(identifier);
        Token token;
        if (optionalToken.isPresent()) {
            token = optionalToken.get();
        } else {
            token = new Token();
            token.setIdentifier(identifier);
        }
        token.setToken(csrfToken.getToken());
        jpaTokenRepository.save(token);
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest httpServletRequest) {
        String identifier = httpServletRequest.getHeader("X-IDENTIFIER");
        Optional<Token> optionalToken = jpaTokenRepository.getTokenByIdentifier(identifier);
        if (optionalToken.isPresent()) {
            return new DefaultCsrfToken("X-CSRF-TOKEN", "_CSRF", optionalToken.get().getToken());
        }
        return null;
    }
}
