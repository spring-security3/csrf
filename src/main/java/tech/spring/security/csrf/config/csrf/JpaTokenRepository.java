package tech.spring.security.csrf.config.csrf;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JpaTokenRepository extends JpaRepository<Token, Integer> {

    Optional<Token> getTokenByIdentifier(String identifier);
}
